// Name: Sittisak Choolak
// Student ID: 6450110013

class RegisterDataModel {
  final String reportUser, reportEmail, reportPassword;

  RegisterDataModel(
      this.reportUser,
      this.reportEmail,
      this.reportPassword);
}
